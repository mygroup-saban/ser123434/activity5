========================================> ANALYSIS
-- Table Name: classic_models

-- TTABLES AND FIELDS
Table: customers
Field: 
        customerNumber(primary key) 
        salesRepEmployeeNumber (foreign key) int(11) to employees(employeeNumber) in customers_ibfk_1
        customerName varchar(50)
        contactLastName varchar(50)
        contactFirstName varchar(50)
        phone varchar(50)
        addressLine1 varchar(50)
        addressLine2 varchar(50)
        city varchar(50)
        state varchar(50)
        postalCode varchar(15)
        country varchar(50)
        creditLimit decimal(10,2)
Table: employees
Field: 
        employeeNumber(primary key)
        lastName varchar(50)
        firstName varchar(50)
        extension varchar(10)
        email varchar(100)
        officeCode int(11) to offices(officeCode) in employees_ibfk_1
        reportsTo int(11) to employees(employeeNumber) in employees_ibfk_2
        jobTitle varchar(50)

Table: offices
Field: 
        officeCode(primary key)
        city varchar(50)
        phone varchar(50)
        addressLine1 varchar(50)
        addressLine2 varchar(50)
        state varchar(50)
        country varchar(50)
        postalCode varchar(15)
        territory varchar(10)

Table: orderdetails
Field: 
        orderNumber(primary key) int(11) to orders(orderNumber) in orderdetails_ibfk_1
        productCode(primary key) varchar(15) to products(productCode) in orderdetails_ibfk_2
        quantityOrdered int(11)
        priceEach decimal(10,2)
        orderLineNumber smallint(6)

Table: orders
Field: 
        orderNumber(primary key)
        orderDate date
        requiredDate date
        shippedDate date
        status varchar(15) 
        comments varchar(65535)
        customerNumber int(11) to customers(customerNumber) in orders_ibfk_1

Table: payments
Field: 
        customerNumber(primary key) int(11) to customers(customerNumber) in payments_ibfk_1
        checkNumber(primary key) varchar(50)
        paymentDate date
        amount decimal(10,2)

Table: productlines
Field: 
        productLine(primary key)
        textDescription varchar(4000)
        htmlDescription mediumtext
        image mediumblob

Table: products
Field: 
        productCode(primary key)
        productName varchar(70)
        productLine varchar(50) to productlines(productLine) in products_ibfk_1
        productScale varchar(10)
        productVendor varchar(50)
        productDescription text
        quantityInStock smallint(6)
        buyPrice decimal(10,2)
        MSRP decimal(10,2)

=============================================================>SOLUTIONS

-- MySQL S5 Activity:

USE classic_models;

-- 1. Return the customerName of the customers who are from the Philippines.
answer:
SELECT customerName FROM customers WHERE country = 'Philippines';



-- 2. Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"
answer:
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = 'La Rochelle Gifts';


-- 3. Return the product name and MSRP of the product named "The Titanic"
answer:
SELECT productName, MSRP FROM products WHERE productName = 'The Titanic';




-- 4. Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"
answer:
SELECT firstName, lastName FROM employees WHERE email = 'jfirrelli@classicmodelcars.com';



-- 5. Return the names of customers who have no registered state
answer:
SELECT customerName FROM customers WHERE state IS NULL;


-- 6. Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve
answer:
SELECT firstName, lastName, email FROM employees WHERE lastName = 'Patterson' AND firstName = 'Steve';



-- 7. Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000
answer:
SELECT customerName, country, creditLimit FROM customers WHERE country != 'USA' AND creditLimit > 3000;




-- 8. Return the customer numbers of orders whose comments contain the string 'DHL'
answer:
SELECT customerNumber FROM orders WHERE comments LIKE '%DHL%';




-- 9. Return the product lines whose text description mentions the phrase 'state of the art'
answer:
SELECT productLine FROM productlines WHERE textDescription LIKE '%state of the art%';




-- 10. Return the countries of customers without duplication
answer:
SELECT DISTINCT country FROM customers;



-- 11. Return the statuses of orders without duplication
answer:
SELECT DISTINCT status FROM orders;



-- 12. Return the customer names and countries of customers whose country is USA, France, or Canada
answer:
SELECT customerName, country FROM customers WHERE country IN ('USA', 'France', 'Canada');

-- 13. Return the first name, last name, and office's city of employees whose offices are in Tokyo
answer:
SELECT firstName, lastName, city FROM employees JOIN offices ON employees.officeCode = offices.officeCode WHERE city = 'Tokyo';



-- 14. Return the customer names of customers who were served by the employee named "Leslie Thompson"
answer:
SELECT customerName FROM customers JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber WHERE firstName = 'Leslie' AND lastName = 'Thompson';


-- 15. Return the product names and customer name of products ordered by "Baane Mini Imports"
answer:
SELECT productName, customerName FROM products 
    JOIN orderdetails ON products.productCode = orderdetails.productCode 
    JOIN orders ON orderdetails.orderNumber = orders.orderNumber 
    JOIN customers ON orders.customerNumber = customers.customerNumber 
    WHERE customerName = 'Baane Mini Imports';



-- 16. Return the employees' first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are in the same country
answer:
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM employees 
    JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber 
    JOIN offices ON employees.officeCode = offices.officeCode 
    WHERE customers.country = offices.country;

-- 17. Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000
answer:
SELECT productName, quantityInStock FROM products 
    JOIN productlines ON products.productLine = productlines.productLine 
    WHERE productlines.productLine = 'Planes' AND quantityInStock < 1000;


-- 18. Show the customer's name with a phone number containing "+81".
answer:
SELECT customerName FROM customers WHERE phone LIKE '%+81%';




